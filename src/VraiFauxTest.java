import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static java.lang.System.lineSeparator;

public class VraiFauxTest {

    @Test
    void ShouldAskVraiFauxQuestion() {
        Question qTest = new VraiFaux("la capitale de la France est Paris", true);

        Assertions.assertEquals("[VRAI/FAUX]" + lineSeparator() + "la capitale de la France est Paris", qTest.askQuestion());
    }

    @Test
    void ShouldAnswerOne() {
        Question qTest = new VraiFaux("la capitale de la France est Paris", true);
        qTest.askQuestion();
        String userAnswer = "true";
        Assertions.assertEquals(1, qTest.compareRep(userAnswer));
    }

    @Test
    void ShouldAnswerZeroWithInvalidInput() {
        Question qTest = new VraiFaux("la capitale de la France est Paris", true);
        qTest.askQuestion();
        String userAnswer = "vrai";
        Assertions.assertEquals(0, qTest.compareRep(userAnswer));
    }

    @Test
    void ShouldAnswerZeroWithWrongInput() {
        Question qTest = new VraiFaux("la capitale de la France est Paris", true);
        qTest.askQuestion();
        String userAnswer = "false";
        Assertions.assertEquals(0, qTest.compareRep(userAnswer));
    }
}
