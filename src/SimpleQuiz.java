import java.util.Scanner;

public class SimpleQuiz {

    public static void main(String[] args) {


        String name;
        Scanner s = new Scanner(System.in);
        //initialise le score
        Score score = new Score();
        //initialise le manager
        Manager manager = new Manager();

        //question prénom
        System.out.println("Quel est votre nom ?");
        name = s.nextLine();


        //intialiser la question
        for (Question q : manager.getListOfQuestions()) {
            //poser la question
            System.out.println(q.askQuestion());
            String reponse = readAnswer(s);
            //comparer la réponse et mettre à jour le score
            score.updateScore(q.compareRep(reponse));
            System.out.println("[score = " + score.getScore() + "]");
        }

        System.out.println(name + "Ton score est de " + score.getScore());
    }

    //FIN DU MAIN
    public static String readAnswer(Scanner s) {
         return s.nextLine();
    }
}
