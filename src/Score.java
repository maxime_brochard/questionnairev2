public class Score {

    private int value = 0;
    Manager manager = new Manager();

    public Score() {
    }
//Incrémentation du score à chaque réponse
    public void updateScore(int updateValue) {this.value += updateValue;

    }
//Impression du résultat final (pour une execution du programme)
    public int getScore() {
        return this.value;
    }
}
