import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static java.lang.System.lineSeparator;

public class QuestionTest {

    @Test
    void ShouldAskQuestion() {
        Question qTest = new Question("la réponse à la vie?", "42");
        Assertions.assertEquals("la réponse à la vie?", qTest.askQuestion());
    }

    @Test
    void ShouldGetQuestion() {
        Question qTest = new Question("la réponse à la vie?", "42");
        Assertions.assertEquals("la réponse à la vie?", qTest.getQuestion());
    }

    @Test
    void ShouldGetReponse() {
        Question qTest = new Question("la réponse à la vie?", "42");
        Assertions.assertEquals("42", qTest.getReponse());
    }

    @Test
    void answerShouldBeRight() {
        Question qTest = new Question("la réponse à la vie?", "42");
        Assertions.assertEquals(1, qTest.compareRep("42"));
    }

    @Test
    void answerShouldBeRightWithPartialText() {
        Question qTest = new Question("la capitale de la france ?", "Paris");
        Assertions.assertEquals(1, qTest.compareRep("Paris"));
    }

    @Test
    void answerShouldBeWrong() {
        Question qTest = new Question("la réponse à la vie?", "42");
        Assertions.assertEquals(0, qTest.compareRep("3"));
    }

    @Test
    void shouldMatchDistance() {
        Question question = new Question("Mon Prénom ?", "Maxime");
        Assertions.assertEquals(2, question.levenshteinDistance(question.reponse, "Mame")); ;
    }

    @Test
    void shoulNotdMatchDistance() {
        Question question = new Question("Mon Prénom ?", "Maxime");
        Assertions.assertNotEquals(10, question.levenshteinDistance(question.reponse, "Mame")); ;
    }
}