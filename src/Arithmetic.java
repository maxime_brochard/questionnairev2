import java.util.concurrent.ThreadLocalRandom;

public class Arithmetic extends Question {
    //Attributs pour Arithmetic
    private int num1 = 0, num2 = 0;
    private final int min = 1, max = 10;
    private char operators[] = new char[]{'+', '-', '*'};
    private char operator;
    private int result;

    public Arithmetic() {

            this.num1 = defineNumber();
            this.num2 = defineNumber();
            this.operator = defineOperator();
            this.result = Mresult();
            //Conversion de l'attribut resultat du calcul pour être passer en paramètre String de la question et comparer à l'entrée user en String
            this.reponse=Integer.toString(result);
            this.question= "quel est le résultat de : " + num1 + " " + operator + " " + num2 + " ?";
    }

    protected String askQuestion() {
        return question;
    }

    @Override
    protected int compareRep(String UserAnswer) {
        if (UserAnswer.equals(this.reponse)) {
            return 1;
        } else {
            return 0;
        }
    }

    //Méthode de définition d'un nombre aléatoire
    private int defineNumber() {
        int number = ThreadLocalRandom.current().nextInt(min, max + 1);
        return number;
    }
    //Méthode de définition d'un opérateur aléatoire
    private char defineOperator() {
        char res = operators[(int) (Math.random() * operators.length)];
        return res;
    }
    //Méthode de calcul et de retour du résultat suivant l'opérateur et les opérandes executés
    private int Mresult() {
        switch (operator) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '/':
                result = num1 / num2;
                break;
            case '*':
                result = num1 * num2;
                break;
        }
        return result;

    }
}


