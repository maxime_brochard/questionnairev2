import static java.lang.System.lineSeparator;

public class VraiFaux extends Question {
    private String prefix = "[VRAI/FAUX]";

    //initalise une variable reponse de type boolean
    protected boolean reponse;

    public VraiFaux(String question, boolean reponse) {
        this.question = question;
        this.reponse = reponse;
    }

    @Override
    public String askQuestion() {
        return this.prefix + lineSeparator() + super.askQuestion();
    }

    @Override
    protected int compareRep(String userAnswer) {

        if (Boolean.parseBoolean(userAnswer) == reponse) {
            return 1;
        } else {
            return 0;
        }
    }

}

