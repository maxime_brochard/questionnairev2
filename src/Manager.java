import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Manager {

    //Initialise le nombre d'itérations de la question arithmetique
    private int numberOfArithmeticQuestions;

    //Appel de la list intégrant les différentes questions
    private List<Question> ListQuestions = new ArrayList<>();

    public Manager() {
        
        ListQuestions.add(new VraiFaux("le mt st michel Breton.", true));
        ListQuestions.add(new Question("Qui est le chien de tintin", "milou"));
        ListQuestions.add(new QCM("Comment s'appelle le dernier véhicule de Tesla?", "Cybertruck", List.of("Cyberpunk", "Cybertruck", "Cybières")));
        // Création question arithmétique
        this.numberOfArithmeticQuestions = 3;
        for (int i = 0; i < numberOfArithmeticQuestions; i++) {
            ListQuestions.add(new Arithmetic());
        }
    }


    //getter
    public List<Question> getListOfQuestions() {
        return Collections.unmodifiableList(this.ListQuestions);
    }


}


