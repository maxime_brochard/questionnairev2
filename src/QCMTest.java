import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;

public class QCMTest {
    Question qTest= new QCM("quelle est là couleur du cheval d'Henri IV?", "blanc", List.of("bleu", "blanc", "rouge"));

    @Test
    void ShouldReturnQCMQuestion() {
        Assertions.assertEquals("quelle est là couleur du cheval d'Henri IV?" + lineSeparator() + "0) bleu" + lineSeparator() +"1) blanc" + lineSeparator() +"2) rouge", qTest.askQuestion());
    }

    @Test
    void ShouldBeGoodWithGoodText() {
        String userAnswer = "blanc";
        Assertions.assertEquals(1, qTest.compareRep(userAnswer));
    }


    @Test
    void ShouldBeWrongWithWrongText() {
        String userAnswer = "orange";
        Assertions.assertEquals(0, qTest.compareRep(userAnswer));
    }

    @Test
    void ShouldBeGoodWithPartialText() {
        ArrayList<String>ListChoicesTest = new ArrayList<>();
        ListChoicesTest.add("bleu");
        ListChoicesTest.add("blanc");
        ListChoicesTest.add("rouge");

        Question qTest = new QCM("quelle est là couleur du cheval d'Henri IV?", "blanc", ListChoicesTest);

        String userAnswer = "blan";

        Assertions.assertEquals(1, qTest.compareRep(userAnswer));
    }

    @Test
    void ShouldBeWrongWithWrongIndex() {
        String userAnswer = "0";
        Assertions.assertEquals(0, qTest.compareRep(userAnswer));
    }

    @Test
    void ShouldBeGoodWithGoodIndex() {
        String userAnswer = "1";
        Assertions.assertEquals(1, qTest.compareRep(userAnswer));
    }
}

