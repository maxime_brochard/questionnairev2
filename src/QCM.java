import java.util.List;

import static java.lang.System.lineSeparator;

public class QCM extends Question {
    private List choices;

    //Constructeur
    public QCM(String question, String reponse, List choices) {
        super(question, reponse);
        this.choices = choices;
    }

    @Override
    // propositions disponibles dans la liste de choix avec leur numéro d'index
    protected String askQuestion() {
        String question = super.askQuestion();
        String choix = "";
        for (int i = 0; i < choices.size(); ++i) {
            choix += choix = lineSeparator() + i + ") " + choices.get(i);
        }
        return question + choix;
    }

    @Override
    //avec la possibilité de comparer sur l'index de la réponse
    protected int compareRep(String userAnswer) {
        if (userAnswer.toLowerCase().equals(this.reponse.toLowerCase()) || userAnswer.equals(Integer.toString(choices.indexOf(reponse)))) {
            return 1;
        } else {
            return 0;
        }
    }
}

